(defproject incremintal "0.1.0-SNAPSHOT"
  :description "WIP: Library - with examples - for creating incremental games"
  :url "https://pete23.com/incremintal"
  :license {:name "AGPL-3.0-only"
            :url "https://www.gnu.org/licenses/agpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.10.2"]]
  :repl-options {:init-ns pete23.incremintal})
