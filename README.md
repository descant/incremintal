# incremintal

A library for developing incremental games with an example.

Really a way of getting these ideas out of my head and onto virtual
paper so that they stop bothering me.

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## What is an "Incremental Game"?

https://en.wikipedia.org/wiki/Incremental_game

_In an incremental game, a player performs a simple action – usually clicking a button – which in turn rewards the player with currency. The player may spend currency to purchase items or abilities that allow the player to earn currency faster or automatically, without needing to perform the initial action._

## Why is this sufficiently interesting to write a library for?

By playing and observing the mechanics of several of these games, you
can see that there are certain common concepts at work. Having built
one (with my son, on Xmas Eve, at work - so behind a firewall, sadly,
but getting playtest and rapid feedback from bank execs was very
entertaining), I found that writing the mechanical code gets
complicated quickly. That complication takes away from the fun piece,
which is really the story you are telling with the interaction of the
different components.

Taking a considered approach to this it becomes obvious that things
can be decomposed into two functions, both of which take world-state
and return world-state.

**(click state & clicks)**

Take the state of the world and apply the given clicks.

**(tick state)**
**(tick state ticks)**

Take the state of the world and age it by one or more "ticks".

Bearing in mind that an idle user may not pick up an application for
some time, or that we might want to show a graph projection, a simple
iterative implementation of tick might not be practical. I've been
chewing on how an integration approach can be used but not really
grounded it outside my head. Hence this library.

**(render state)**

1,000,000,000,000,000,000 is 1 quintillion, 1q, 1 exa, 1E, 1*10^18,
1E18.

It will be very common to go from a 1N bigdec representation to
something more friendly: "1 fruitbat","1,000 fruitbats", "1 million
fruitbats", "1.23 exafruitbats". This rendering may depend on the
chosen UI, and might not need library filtering at all. That said,
we'll supply one out of the box that makes prototyping with this
library directly ergonomically pleasant.

**(repl state)**

Read from keyboard, Evaluate ticks based on wall-clock time, clicks
based on input, print, loop. Again, probably unnecessary if you have a
UI to drive clicks'n'ticks according to your reified Will, but a handy
affordance for command-line testing.

## Code of Conduct

In the event that we have any sort of interaction with anyone at all
regarding this library, I feel it's important to get the ground rules
in at the start.

https://www.contributor-covenant.org/version/2/0/code_of_conduct/

From their remarkably unsnarky FAQ:

_Doesn’t this code of conduct just promote political correctness?
Only if you define political correctness as the belief that women,
non-binary people, gay, lesbian, queer, and/or transgender people,
people of color, and people of different religious backgrounds should
be afforded the same rights and privileges as everyone else._

If you can't play nicely with other people, I'd prefer it if you
choose not to play here.

## Licensing Thoughts

This is likely to be a small piece of work, assuming it ever becomes
functionally useful. There will be very little stopping people from
reimplementing it, or in the unlikely event of me coming across novel
Dopamine pathway exercise IP copying the One Cool Trick.

Alignment of the "Free (as in beer) and Open Source Software" movement
with my own political and moral positions is problematic. I believe
the Commons should be a two way process of contribution and
consumption, not a place for large companies to gank free stuff in a
one way deal. I'm working at the place I work to get the contribution
pathways in place, at least. The road is long and hard but we'll get
there. What are you doing?

The AGPL is therefore a statement of intent - I want to make it easy
for the makers of Free Software to reuse this library to make games. I
want to make it easy for the makers of Free Game Servers if there are
such things to reuse this library to make their games. I do not want
people who are not willing to free the source for their own games to
use this library for free.

## License

Copyright © 2021 Pete Windle

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
